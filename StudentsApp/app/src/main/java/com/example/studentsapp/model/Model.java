package com.example.StudentsApp.model;

import java.util.LinkedList;
import java.util.List;

public class Model {
    public static final Model instance = new Model();

    private Model() {
//        for (int i = 0; i < 20; i++) {
//            Student s = new Student("name " + i, "id " + i, "" + i, "bla bla street " + i, false);
//            data.add(s);
//        }

        data.add(new Student("Jonathan", "1111111", "0547772623", "Kfar Shmarihau", false));
        data.add(new Student("Itamar", "2222222", "052321323", "Bizaron", false));

    }

    List<Student> data = new LinkedList<Student>();

    public List<Student> getAllStudents() {
        return data;
    }

    public Student getStudent(int position) {
        return data.get(position);
    }

    public void addStudent(Student student) {
        data.add(student);
    }

    public void removeStudent(int position) {
        data.remove(position);
    }
}
